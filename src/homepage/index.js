var page = require('page');
var empty = require('empty-element');
var template = require('./template');
var title = require('title');

page('/', function (ctx, next) {
  title('Platzigram')
  var main = document.getElementById('main-container');

  var pictures = [
    {
      user: {
        username: 'ichikawaROB',
        avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/p160x160/10288749_982109105213936_6245451542496872849_n.jpg?oh=ae3bf8558b95a57405cfbbf1f7ecd73a&oe=580FE02B&__gda__=1473950359_9d1320bdb8057663074f8bdd89962bd3'
      },
      url: 'office.jpg',
      likes: 10,
      liked: false,
      createdAt: new Date()
    },
    {
      user: {
        username: 'ichikawaROB',
        avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/p160x160/10288749_982109105213936_6245451542496872849_n.jpg?oh=ae3bf8558b95a57405cfbbf1f7ecd73a&oe=580FE02B&__gda__=1473950359_9d1320bdb8057663074f8bdd89962bd3'
      },
      url: 'office.jpg',
      likes: 2,
      liked: true,
      createdAt: new Date().setDate(new Date().getDate() - 10)
    }
  ]

  empty(main).appendChild(template(pictures));
})
